About
=====
This script may be uploaded onto a node and run as a 15-minute cronjob for
monitoring a Freifunk node's access to the internet. You may edit a few variables inside the script
depending on your hood's SSID and what target on you want to be checked. This may be a normal FQDN
or an IP-address like the google.com name server (8.8.8.8) or your router's gateway IP if you're doing
mesh-on-wan.

Installation
============
	1.) Copy the script to a prefered directory on your node via sftp or wget. I recommend /usr/share/
	2.) Do a chmod +x <filename> on the script
	3.) Type 'crontab -e' and add a new line: */15 * * * * /<path_to_script>/keepalive.sh 2>&1
	4.) Type /etc/init.d/cron reload
	