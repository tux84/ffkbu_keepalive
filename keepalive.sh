#!/bin/sh

### Freifunk connection keep-alive script
### (c) 2016 Julian Zielke <jzitms@gmail.com>
### Developed for Freifunk Köln Bonn

### Global vars ###

VRFYHOST='google.de' # which host to ping for verification
SSID='kbu.freifunk.net' # SSID for this hood
REBOOT=1 # set to 1 to reboot router after n seconds of waiting defined by $RECONWAIT
RECONWAIT=180 # time to wait until second try is done before reboot

### Do not edit below here ##

# check if necessary pkgs are present on target
if ! [ "`which uci`" ]; then # missing uci bin
  echo "Uci bin not found on system!"
  exit 1
elif ! [ "`which wifi`" ]; then # missing wifi bin
  echo "Wifi bin not found on system!"
  exit 1
elif [ $(uci show gluon-setup-mode.@setup_mode[0].enabled | grep -c "='1") -eq 1 ]; then # check if route in config mode to prevent reboot
  echo "Config mode enabled! Skipping..."
  exit 1
fi

if [ $(ping -w 5 -c 3 $VRFYHOST | grep "bytes from" | wc -l) -eq 0 ]; then
  echo "Node has lost connection to ${VRFYHOST} - changing SSID to $SSID-offline..."
  uci set wireless.client_radio0.ssid="$SSID-offline" # set new SSID
  uci commit wireless # commit the change
  echo "Restarting wifi stack..."
  wifi # reinitialize wifi stack   
  echo "Waiting $RECONWAIT seconds..."
  sleep $RECONWAIT
  if [ $(ping -w 5 -c 1 $VRFYHOST | grep "bytes from" | wc -l) -eq 0 ] && [ $REBOOT -eq 1 ]; then
    echo "Rebooting node..."
    reboot
  fi
else
  if [ $(uci show wireless.client_radio0.ssid | grep -c "${SSID}-offline") -eq 1 ]; then # connection established again
    echo "Node connection established to $VRFYHOST - changing SSID to ${SSID}..."
    uci set wireless.client_radio0.ssid="${SSID}" # set new SSID
    uci commit wireless # commit the change
    echo "Restarting wifi stack..."
    wifi # reinitialize wifi stack
  fi
  echo "Connection ok!"
fi
